"use strict";

/*Завдання
Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

  Технічні вимоги:
1. Отримати за допомогою модального вікна браузера два числа.
2. Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
3. Створити функцію, в яку передати два значення та операцію.
4. Вивести у консоль результат виконання функції.
  Необов’язкове завдання підвищеної складності
5. Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, – запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).*/

let numFirst;
let numSecond;
let mathOperation;

do {
  numFirst = parseFloat(prompt("Введіть перше число:", numFirst));
} while (isNaN(numFirst) || numFirst === "" || numFirst === null);

do {
  numSecond = parseFloat(prompt("Введіть друге число:", numSecond));
} while (isNaN(numSecond) || numSecond === "" || numSecond === null);

do {
  mathOperation = prompt("Введіть арифметичний знак ( + , - , * , / ), який виконає математичну операцію:");
} while (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '*' && mathOperation !== '/');

function calcResult(numFirst, numSecond, mathOperation) {
  switch (mathOperation) {
    case '+':
      return numFirst + numSecond;
    case '-':
      return numFirst - numSecond;
    case '*':
      return numFirst * numSecond;
    case '/':
      if (numSecond !== 0) {
        return numFirst / numSecond;
      } else {
        return "Дана операція не допустима!";
      }
    default:
      return "Помилка: операція не визначена";
  }
}

console.log(calcResult(numFirst, numSecond, mathOperation));